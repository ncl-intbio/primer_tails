# Primer Tails design tool

This is a script to design tails for tagging primers.

Tails are single-stranded DNA, designed to sit 5' of PCR primers.
They are linked to PCR primers by a 3-carbon linker, and should not take part in the PCR process.
PCR products are therefore produced with a 5' tail overhang.
Once the PCR process has gone to completion, these tails can be hybridised to fish out or label the product.

Tails have some constraints.

* They must not have any secondary structure elements
  * Internally
  * or when attached to their primer(s)
* They must not hybridise with any DNA that may be in the sample

This script generates random tail oligos, validating that they do not contain:
* internal secondary structure elements using Primer3
* high-a regions
* secondary structure elements once joined to each other primer in turn,
* or to their reverse complement (corresponding to the sequences used to fish the probes)
* homology to the NCBI nucleotide database

![workflow diagram](mermaid-diagram-2023-09-13-211427.png)

## Running from within SBT

Assuming that the project is checked out to `/home/nmrp3/devel/ncl/primer_tails`,
the script can be compiled and run from a git checkout as follows:

```
>sbt
sbt:primerTails> run -l 15 -n 10 -p "/home/nmrp3/devel/ncl/primer_tails/data/primers.fa" -g "/home/nmrp3/devel/ncl/primer_tails/data/genes"
```

This will produce a large amount of logging output, terminating in a final report similar to this:

```
Solution:
1,caccggcgcggaaac
2,cgacgtaggggggaa
3,ggcgcaccgtctgca
4,tcattcgttcctcgg
5,aggcaccttggatac
6,gcttcgtcgtcgacg

Rejected:
1,tccatcatatcgtaa,structure in gtctaagaaaattcatccatcatatcgtaa,structure in tgaattttcttagactccatcatatcgtaa,structure in tccatcatatcgtaactatcctagatagaa,structure in ttaagatacagactatccatcatatcgtaa,structure in tagtctgtatcttaatccatcatatcgtaa
2,ttaagatacagacta,structure in tcccgagcactatagttaagatacagacta,structure in ttaagatacagactatcccgagcactatag,structure in ctatagtgctcgggattaagatacagacta,structure in ttaagatacagactatccatcatatcgtaa,structure in tagtctgtatcttaatccatcatatcgtaa
3,gtctaagaaaattca,High-A segment,structure in gtctaagaaaattcactatcctagatagaa,structure in gtctaagaaaattcatccatcatatcgtaa,structure in tgaattttcttagactccatcatatcgtaa,structure in gtctaagaaaattcatcccgagcactatag,structure in tgaattttcttagactcccgagcactatag
4,tcccgagcactatag,structure in ctatcctagatagaatcccgagcactatag,structure in tcccgagcactatagttaagatacagacta,structure in ttaagatacagactatcccgagcactatag,structure in ctatagtgctcgggattaagatacagacta,structure in gtctaagaaaattcatcccgagcactatag,structure in tgaattttcttagactcccgagcactatag
5,ctatcctagatagaa,structure in ctatcctagatagaagttgtcagatatttttccgag,structure in gtctaagaaaattcactatcctagatagaa,structure in ctatcctagatagaatcccgagcactatag,structure in tccatcatatcgtaactatcctagatagaa
```

You can copy-paste this into a spreadsheet and use the comas to separate it into distinct columns.

## Command-line

The options for the application are as follows:

```
program_tails 0.1.1
Usage: random_tails [options]

  -m, --minLength <value>  minimum length of tail sequences
  -M, --maximumLength <value>
                           maximum length of tail sequences
  -l, --length <value>     length of tail sequences
  -s, --seed <value>       random number seed
  -n, --number <value>     number of sequences to generate
  -p, --primerFile <value>
                           primers to validate against
  -g, --genesDirectory <value>
                           genes to design against
  -b, --runBlast <value>   run blast checks
```

Lastly, the project can be packaged as a binary. For example, as a zip archive:

The project can be packaged as a zip archive as follows:


```
sbt:primerTails> universal:packageBin
```

This will build binaries that can be installed on another system.