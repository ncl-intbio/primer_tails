val dottyVersion = 
  dottyLatestNightlyBuild.get // autofetched nightly - doesn't compile though
//  "0.14.0-bin-20190317-b537220-NIGHTLY" // works

lazy val root = project
  .enablePlugins(JavaAppPackaging)
  .in(file("."))
  .settings(
    name := "primerTails",
    version := "0.1.0",
    maintainer := "turingatemyhamster@gmail.com",

    scalaVersion := dottyVersion,

    libraryDependencies += ("org.typelevel" %% "cats-core" % "1.6.0").withDottyCompat(scalaVersion.value),

    libraryDependencies += ("com.github.scopt" %% "scopt" % "4.0.0-RC2").withDottyCompat(scalaVersion.value),

    libraryDependencies += "com.novocode" % "junit-interface" % "0.11" % "test",
//    libraryDependencies += ("org.scalacheck" %% "scalacheck" % "1.14.0" % "test").withDottyCompat(scalaVersion.value)
//    libraryDependencies += ("org.scalactic" %% "scalactic" % "3.0.5" % "test").withDottyCompat(scalaVersion.value)

//    libraryDependencies ++= Seq(
//      ("org.scalactic" %% "scalactic" % "3.0.5" % "test").withDottyCompat(scalaVersion.value),
//      ("org.scalatest" %% "scalatest" % "3.0.5" % "test").withDottyCompat(scalaVersion.value))
//    
//    libraryDependencies += ("com.lihaoyi" %% "utest" % "0.6.6" % "test").withDottyCompat(scalaVersion.value),
//    testFrameworks += new TestFramework("utest.runner.Framework")
  )
  .settings(
    mainClass in Compile := Some("primerTails.RandomTails"),
    packageName in Universal := "primer_tails"
  )

