addSbtPlugin("ch.epfl.lamp" % "sbt-dotty" % "0.3.0")

addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.3.20")