package bio

import org.junit._
import Assert._

class FataSpec {

  @Test def parseIdOnly(): Unit = {
    val oneLine = ">id"

    val olFA = Fasta.parse(oneLine)
    assertEquals(olFA, Fasta(">id", List())::Nil)

    val oneLineNL = ">id\n"
    val olnFa = Fasta.parse(oneLineNL)
    assertEquals(olnFa, Fasta(">id", List())::Nil)

    val oneLineBlankNL = ">id\n\n"
    val olnbFa = Fasta.parse(oneLineBlankNL)
    assertEquals(olnbFa, Fasta(">id", List())::Nil)

    val leadingNL = "\n>id"
    val leadingFa = Fasta.parse(leadingNL)
    assertEquals(leadingFa, Fasta(">id", List())::Nil)
  }
}