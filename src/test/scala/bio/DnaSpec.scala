package bio

import org.junit._
import Assert._

import ct._
import implied ct._

class DnaSpec {

  @Test def testDNAParse(): Unit = {
    val gataca = "gataca"
    val dna = gataca.toDna.orThrow

    val dnaStr = dna.render
    assertEquals(dnaStr, gataca)
  }

  @Test def testDNARevComp(): Unit = {
    val gataca = "gataca"
    val dna = gataca.toDna.orThrow

    val rev = dna.reverse
    assertEquals(rev.render, "acatag")

    val cmp = dna.complement
    assertEquals(cmp.render, "ctatgt")

    val revCmp = rev.complement
    val cmpRev = cmp.reverse
    assertEquals(revCmp, cmpRev)

    val undo = revCmp.reverseComplement
    assertEquals(undo, dna)
  }

  @Test def testReverseStrand(): Unit = {
    val gataca = "gataca"
    // fixme: had to reverse the argument order for no discernible reason
    val revStrandDNA = toBiopolymer[Dna_ReverseStrand](gataca).orThrow
    //val revStrandDNA = gataca.toBiopolymer[Dna_ReverseStrand].orThrow

    assertEquals(revStrandDNA.render, gataca)
    assertDifferentType(revStrandDNA, gataca.toDna) // should be 3'->5' vs 5'->3'
  }

  @Test def testSubsequence(): Unit = {
    val gataca = "gataca"
    val dna = gataca.toDna.orThrow

    val tac = dna.subregion(3, 5)
    assertEquals(tac.render, "tac")
  }

  def assertSameType[A, B](a: A, b: B) given SameType[A, B] = the[SameType[A, B]].check
  def assertDifferentType[A, B](a: A, b: B) given DifferentType[A, B] = the[DifferentType[A, B]].check

  trait SameType[S, T] {
    def check: Unit
  }

  implied [A]    for SameType[A, A] { def check = {} }
  implied [A, B] for SameType[A, B] { def check = Assert.fail("Expected both arguments to be the same type") }

  trait DifferentType[S, T] {
    def check: Unit
  }

  implied [A]    for DifferentType[A, A] { def check = Assert.fail("Expected both arguments to be different types") }
  implied [A, B] for DifferentType[A, B] { def check = {} }
}