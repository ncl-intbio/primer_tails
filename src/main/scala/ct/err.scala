package ct

import cats._
import cats.implicits._

opaque type Err[T] = Either[String, T]

object Err {

  private[ct] inline def wrap[T](e: Either[String, T]): Err[T] = e
  private[ct] inline def unwrap[T](e: Err[T]): Either[String, T] = e
}

implied ErrMonad for Monad[Err] {
  type EitherErr[T] = Either[String, T]

  def pure[A](x: A): Err[A] =
    Err.wrap(the[Monad[EitherErr]].pure(x))
    
  def flatMap[A, B](fa: Err[A])(f: A => Err[B]): Err[B] = 
    Err.wrap(
      the[Monad[EitherErr]].flatMap(
        Err.unwrap(fa))(a => Err.unwrap(f(a))))

  def tailRecM[A, B](a: A)(f: A => Err[Either[A, B]]): Err[B] = Err.unwrap(f(a)) match {
    case Left(err) => Err.wrap(Left(err))
    case Right(Left(nextA)) => tailRecM(nextA)(f)
    case Right(Right(b)) => Err.wrap(Right(b))
  }
}

implied ErrSyntax {
  def (t: T) success[T]: Err[T] = Err.wrap(Right(t))
  def (msg: String) error[T]: Err[T] = Err.wrap(Left(msg))

  def (e: Err[T]) withError[T](msg: String => String): Err[T] = Err.unwrap(e) match {
    case Left(m) => Err.wrap(Left(msg(m)))
    case Right(t) => Err.wrap(Right(t))
  }

  def (e: Err[T]) recoveringWith[T](rec: => T): Err[T] = Err.unwrap(e) match {
    case Left(m) => Err.wrap(Right(rec))
    case Right(t) => Err.wrap(Right(t))
  }

  def (e: Err[T]) orThrow[T]:T = Err.unwrap(e) match {
    case Left(m) => throw new IllegalStateException(m)
    case Right(t) => t
  }
}