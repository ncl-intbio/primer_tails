package ct

import cats._
import cats.implicits._


def (lhs: T) |+| [T] (rhs: T) given (T: Semigroup[T]): T = T.combine(lhs, rhs)

opaque type Render = Appendable => Unit

object Render {
  def of(a: Appendable => Unit): Render = a

  implied RenderMonoid for Monoid[Render] {
    override def combine(lhs: Render, rhs: Render): Render =
      of(a => { lhs(a) ; rhs(a) })
    override def empty: Render = _ => {}
  }

  private [ct] def unwrap(r: Render): Appendable => Unit = r
}

@FunctionalInterface
trait Rendering[T] {
  def toRender(t: T): Render
}

object Rendering {
  import Render._

  implied for Rendering[Char]         = c => Render.of(_ append c)
  implied for Rendering[CharSequence] = s => Render.of(_ append s)
  implied for Rendering[String]       = s => Render.of(_ append s)

  implied RenderOption[T] given (T: Rendering[T]) for Rendering[Option[T]] =
    _.fold(Render.RenderMonoid.empty)(T.toRender)

  def from[R, S](f: R => S) given (S: Rendering[S]): Rendering[R] = r => S.toRender(f(r))

  def all[R](rs: Iterable[R]) given (R: Rendering[R]): Render = Render.of {
    a => rs.foreach(R.toRender(_).renderTo(a))
  }
}

def (t: T) toRender[T] given (T: Rendering[T]): Render = T.toRender(t)

def (t: T) render[T] given (T: Rendering[T]): String = {
  import Render._

  val sb = new java.lang.StringBuilder
  T.toRender(t) renderTo sb
  sb.toString
}

def (r: Render) renderTo (a: Appendable) = Render.unwrap(r)(a)
