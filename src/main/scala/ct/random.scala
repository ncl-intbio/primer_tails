package ct

import scala.reflect.ClassTag
import scala.collection.generic.CanBuildFrom
import util.Random

/** A non-deterministic value. */
opaque type Rand[T] = Random => T

object Rand {
  def apply[T](t: Random => T): Rand[T] = t
  def unwrap[T](r: Rand[T]): Random => T = r

  def point[T](t: T): Rand[T] = _ => t

  def discrete[T](probs: Seq[(Double, T)]) given ClassTag[T]: Rand[T] = { 
    val (ps, ts) = probs.to[Array].unzip
    var pAcc: Array[Double] = Array.ofDim(ps.length)

    // precompute the accululated probabilities
    {
      var i: Int = 0
      var p: Double = 0.0
      while(i < ps.length) {
        p = p + ps(i)
        pAcc(i) = p
        i = i+1
      }
    }

    // validate that the probabilities sum to 1
    assert(pAcc(pAcc.length - 1) == 1.0)

    // sample value using a linear scan over the accumulated probabilities
    r => {
      val d = r.nextDouble
      var i: Int = 0
      while(pAcc(i) < d) {
        i = i+1
      }

      ts(i)
    }
  }
}
implied RandSyntax {
  def (a: Rand[A]) map [A, B] (f: A => B): Rand[B] = Rand { r => f(a.sample(r))  }

  def (a: Rand[A]) flatMap [A, B] (f: A => Rand[B]): Rand[B] = Rand { r => f(a.sample(r)).sample(r) }

  def (t: Rand[T]) sample[T] (r: Random): T = Rand.unwrap(t)(r)

  def (t: Rand[T]) replicate[T] (n: Int): Rand[List[T]] = Rand { r => List.fill(n)(t.sample(r)) }

  def (t: Rand[T]) forkRand[T]: Rand[T] = Rand { r => 
    val forked = new Random(r.nextLong)
    t.sample(forked)
  }
}

def (t: T) point[T]: Rand[T] = Rand.point(t)

/** A uniform distribution. */
opaque type Uniform[T] = Rand[T]
object Uniform {
  def apply[T](t: Rand[T]): Uniform[T] = t
  def unwrap[T](t: Uniform[T]): Rand[T] = t
}
implied UniformSyntax {
  def (t: Uniform[T]) rand[T]: Rand[T] = Uniform.unwrap(t)
}

def (ts: Iterable[T]) uniform[T] given ClassTag[T]: Uniform[T] = {
  val l = ts.size
  val p = 1.0 / l
  val ps = List.fill(l)(p)
  Uniform(Rand.discrete(ps zip ts))
}


// category
opaque type Conditional[S, T] = S => Rand[T]

object Conditional {
  import Rand._
  def liftC[A, B](f: A => B): Conditional[A, B] = a => Rand.point(f(a))
  def id[S]: Conditional[S, S] = liftC(identity[S])
  def (g: Conditional[B, C]) compose [A, B, C] (f: Conditional[A, B]): Conditional[A, C] = 
    a => f(a) flatMap g
}
