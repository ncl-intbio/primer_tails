package primerTails

import scala.language.implicitConversions
import scala.sys.process._

import ct._
import bio._
import implied bio._


object Primer3 {
  enum Tasks {
    case generic
    case pick_detection_primers
    case check_primers
    case pick_primer_list
    case pick_sequencing_primers
    case pick_cloning_primers
    case pick_discriminative_primers
    case pick_pcr_primers
    case pick_pcr_primers_and_hyb_probe
    case pick_left_only
    case pick_right_only
    case pick_hyb_probe_only
  }
  implied for BoulderValue[Tasks] = _.toString
  implied RBV[V] given Rendering[V] for BoulderValue[V] = _.render

  def checkHairpins[R[_]](primer: Dna) given (B: Boulder[R]) = {
    import B._
    boulder(
      stone(
        "PRIMER_TASK" := Tasks.check_primers,
        "PRIMER_MIN_SIZE" := 5,
        "PRIMER_MAX_SIZE" := 36,
        "SEQUENCE_PRIMER" := primer,
        "PRIMER_THERMODYNAMIC_OLIGO_ALIGNMENT" := 1,
        "PRIMER_MAX_HAIRPIN_TH" := 15,
        "PRIMER_SECONDARY_STRUCTURE_ALIGNMENT" := 1,
        "PRIMER_MAX_SELF_ANY" := 4,
      )
    )
  }

  // def checkPrimingLeft[R[_]](primer: Dna, gene: IupacDna) given (B: Boulder[R]) = {
  //   import B._
  //   boulder(
  //     stone(
  //       "PRIMER_TASK" := "generic",
  //       "PRIMER_PICK_LEFT_PRIMER" := 0,
  //       "PRIMER_PICK_INTERNAL_OLIGO" := 0,
  //       "PRIMER_PICK_RIGHT_PRIMER" := 1,
  //       "SEQUENCE_TEMPLATE" := gene,
  //       "SEQUENCE_PRIMER" := primer
  //     )
  //   )
  // }

  // def checkPrimingRight[R[_]](primer: Dna, gene: IupacDna) given (B: Boulder[R]) = {
  //   import B._
  //   boulder(
  //     stone(
  //       "PRIMER_TASK" := "generic",
  //       "PRIMER_PICK_LEFT_PRIMER" := 1,
  //       "PRIMER_PICK_INTERNAL_OLIGO" := 0,
  //       "PRIMER_PICK_RIGHT_PRIMER" := 0,
  //       "SEQUENCE_TEMPLATE" := gene,
  //       "SEQUENCE_PRIMER_REVCOMP" := primer
  //     )
  //   )
  // }


  def primer3_core(boulderIn: String) = {
    import java.io._
    val in = new ByteArrayInputStream(boulderIn.getBytes)

    Seq("primer3_core", "--strict_tags") #< in
  }


}
