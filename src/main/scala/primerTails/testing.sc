import scala.language.implicitConversions
import scala.sys.process._

import java.nio.file._

import primerTails._

val primerText = new String(
    Files.readAllBytes(
      Paths.get("/home/nmrp3/devel/ncl/primerTails/data/primers.fa")))
val rawPrimers = Fasta.parse(primerText)

val dnaPrimers = rawPrimers.map(_.toDNA)


val blastPrimers = Seq("ssh", "idris@cic-bionode1a", "blastn -db nt -outfmt 6 -num_threads 4") #< 
  Paths.get("/home/nmrp3/devel/ncl/primerTails/data/primers.fa").toFile

val blastOut = blastPrimers.!!
println(blastOut)