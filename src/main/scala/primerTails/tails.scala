package primerTails

import scala.language.implicitConversions

import java.io._
import java.nio.file._
import scala.collection.JavaConverters._
import scala.sys.process._
import scopt.OParser
import cats.implicits._

import ct._
import bio._

object RandomTails {

  def slurp(path: Path): String = new String(
    Files.readAllBytes(
      path))

  def slurp(fileName: String): String = 
    slurp(Paths.get(fileName))

  case class Options(minLength: Int = 10,
                    maxLength: Int = 20,
                    number: Int = 1,
                    primerFile: String = null, // required
                    genesDirectory: String = null,
                    runBlast: Boolean = false,
                    seed: Option[Long] = None)
  {
    lazy val rand = seed.fold(new util.Random())(new util.Random(_))
  }

  val builder = OParser.builder[Options]
  val optionsParser = {
    import builder._
    OParser.sequence(
      programName("random_tails"),
      head("program_tails", "0.1.1"),
      opt[Int]('m', "minLength")
        .action((m, o) => o.copy(minLength = m))
        .text("minimum length of tail sequences"),
      opt[Int]('M', "maximumLength")
        .action((m, o) => o.copy(maxLength = m))
        .text("maximum length of tail sequences"),
      opt[Int]('l', "length")
        .action((l, o) => o.copy(minLength = l, maxLength = l))
        .text("length of tail sequences"),
      opt[Long]('s', "seed")
        .action((s, o) => o.copy(seed = Some(s)))
        .text("random number seed"),
      opt[Int]('n', "number")
        .action((n, o) => o.copy(number = n))
        .text("number of sequences to generate"),
      opt[String]('p', "primerFile")
        .action((p, o) => o.copy(primerFile = p))
        .text("primers to validate against")
        .required(),
      opt[String]('g', "genesDirectory")
        .action((g, o) => o.copy(genesDirectory = g))
        .text("genes to design against")
        .required(),
      opt[Boolean]('b', "runBlast")
        .action((b, o) => o.copy(runBlast = b))
        .text("run blast checks")
    )
  }

  def main(args: Array[String]): Unit = OParser.parse(optionsParser, args, Options()).map { opts =>
      println(s"Running with options $opts")

      val primers = loadPrimers(opts.primerFile).orThrow
      
      println(s"Loaded ${primers.length} primers from ${opts.primerFile}")

      val genes = loadGenes(opts.genesDirectory).orThrow

      println(s"Loaded ${genes.length} genes from within ${opts.genesDirectory}")
      Rendering.all(genes).renderTo(System.out)

      bio.Fasta.RenderFasta[bio.Fasta.Description, bio.IupacDna]

      println(s"Sampling tags with lengths ${opts.minLength}..${opts.maxLength}")
      val lengthDistr = (opts.minLength to opts.maxLength).uniform.rand
      val seqDistr = lengthDistr flatMap Dna.random

      def checkTag(tag: Dna): TagTestResults = {
        val pSeqs = primers.map(_.sequence)
        val gSeqs = genes.map(_.sequence)

        val ps = checkP3Structure(tag, pSeqs)
        def bl = if(opts.runBlast) checkTagPrimersWithBlast(tag, pSeqs) else Nil

        println(s"Checked ${tag.render}")

        if(ps.isPass) ps ++ bl
        else ps
      }

      def checkTags(tag1: Dna, tag2: Dna): TagTestResults =
        checkTagPair(tag1, tag2) :::
        checkTagPair(tag2, tag1) :::
        checkTagPair(tag1.reverseComplement, tag2)

      println("Checking randomised tag sequences")

      val matrixBuilder = MatrixBuilder(checkTag, checkTags)
      import matrixBuilder.appendTag

      val maxFails = opts.number * 2

      def extendWithNewTag(matrix: TestMatrix): Rand[Either[TestMatrix, TestMatrix]] = seqDistr flatMap { tag =>
        println(s"Extending with tag ${tag.render}")
        if(checkTagForPolyN(tag, 4, 5).isFailure) {
          println("Skippinng as it failed with low-complexity region")
          extendWithNewTag(matrix)
        } else {
          val mat = matrix.appendTag(tag)
          val passes = mat.passing.count(_._2 == true)
          val fails = mat.passing.count(_._2 == false)
          println(s"\tpasses: $passes")
          println(s"\tfails: $fails")
          println(s"\ttotal: ${mat.passing.size}")

          if(fails >= maxFails) {
            println("Limit on failures exceeded. No sollution could be found.")
            Left(mat).point
          } else if(passes >= opts.number) {
            println("Found a sollution")
            Right(mat).point
          } else {
            extendWithNewTag(mat)
          }
        }
      }

      val eitherM = extendWithNewTag(TestMatrix.empty).sample(opts.rand)
      println(eitherM)

      eitherM match {
        case Left(_) => println("No solution found. There may be one if you run again.")
        case Right(matrix) =>
          println("Solution:")
          val passes = matrix.passing.collect { case (tag, p) if p => tag }
          passes.zipWithIndex.foreach((t, i) => println(s"${i+1},${t.render}"))

          println()
          println("Rejected:")
          val fails = matrix.passing.collect { case (tag, p) if !p => tag }
          fails.zipWithIndex.foreach { (t, i) =>
            val singles = matrix.singleTests(t)
            val pairs = matrix.pairTests.collect { case ((t1, t2), fs) if t1 == t || t2 == t => fs }
            val failures = (singles ++ pairs.flatten).collect{ case TagTestResult.failed(name, exp) => name }

            println(s"${i+1},${t.render},${failures.mkString(",")}")
          }

          println()
      }
  }

  def loadPrimers(primerFile: String): Err[List[FastaDna]] = Fasta.parse(
    slurp(primerFile))
      .map(_.toDna).sequence

  def loadGenes(genesDirectory: String): Err[List[FastaIupacDna]] = (for {
      faFile <- Files.newDirectoryStream(Paths.get(genesDirectory), "*.{fa,fasta}")
        .asScala.to[List]
      fas <- Fasta.parse(slurp(faFile))
    } yield { fas.toIupacDna.withError(err => s"$err in file $faFile")
    }).sequence



  def checkP3Structure(tag: Dna, primers: List[Dna]): List[TagTestResult] =
    checkOligoStructure(tag) :: checkTagWithAllPrimers(tag, primers)

  def checkTagWithAllPrimers(tag: Dna, primers: List[Dna]): List[TagTestResult] =
    primers.map(checkTagWithPrimer(tag, _))

  def checkTagWithPrimer(tag: Dna, primer: Dna): TagTestResult = {
    println(s"Checking tag ${tag.render} and primer ${primer.render}")
    val combined = tag join primer
    if(combined.length > 36) {
      println("WARNING: trunchated tag and primer to 36 nucleotides")
      checkOligoStructure(combined.subregion(1, 36))
    } else {
      checkOligoStructure(combined)
    }
  }

  def checkTagPair(tag1: Dna, tag2: Dna): TagTestResults = {
    println(s"Checking tag1 ${tag1.render} and tag2 ${tag2.render}")
    val combined = tag1 join tag2
    if(combined.length > 36) {
      println("WARNING: trunchated tag1 and tag2 to 36 nucleotides")
      checkOligoStructure(combined.subregion(1, 36)) :: Nil
    } else {
      checkOligoStructure(combined) :: Nil
    }
  }

  def checkOligoStructure(oligo: Dna): TagTestResult = {
    val boulderIn = Primer3.checkHairpins[[?] => String](oligo)
    val runP3 = Primer3.primer3_core(boulderIn).!!
    val boulderOut = Boulder.parse[[A] => A](runP3)
    
    TagTestResult(s"structure in ${oligo.render}",
      boulderOut.filter(_.get("PRIMER_LEFT_NUM_RETURNED").exists(_ != "0")).isEmpty,
      s"primer3 predicted structural elements: ${boulderOut}")
  }


  def checkTagForPolyN(tag: Dna, atLeast: Int, withinWindow: Int): TagTestResults = {
    println("Checking for high-N segments")
    val freqs = tag.residueFrequencies(withinWindow)
    val highA = freqs.filter(_._2.counts.get(Nucleotide.A).exists(_ >= atLeast))
    val highG = freqs.filter(_._2.counts.get(Nucleotide.G).exists(_ >= atLeast))
    val highC = freqs.filter(_._2.counts.get(Nucleotide.C).exists(_ >= atLeast))
    val highT = freqs.filter(_._2.counts.get(Nucleotide.T).exists(_ >= atLeast))
    val highN = highA ++ highG ++ highC ++ highT

    List(TagTestResult("High-A segment", highA.isEmpty, s"Found high-A windows ${highA}"),
         TagTestResult("High-A segment", highG.isEmpty, s"Found high-A windows ${highG}"),
         TagTestResult("High-A segment", highC.isEmpty, s"Found high-A windows ${highC}"),
         TagTestResult("High-A segment", highT.isEmpty, s"Found high-A windows ${highT}"))
  }
    

  
  def checkTagPrimersWithBlast(tag: Dna, primers: List[Dna]): TagTestResults = 
    primers.map(checkTagPrimerWithBlast(tag, _))

  def checkTagPrimerWithBlast(tag: Dna, primer: Dna): TagTestResult = {
    val tagPrimer = tag join primer
    val tpIS = new ByteArrayInputStream(tagPrimer.render.getBytes)
    val blastCmd = Seq("ssh", "idris@cic-bionode1a", "blastn -db nt -outfmt 6 -num_threads 56") #< tpIS
    print("Blasting... ")
    val blastOut = blastCmd.!!
    println("Done")
    val hits = Blast.parseTabular(blastOut)
    TagTestResult(s"BLAST ${tagPrimer.render} against nt", hits.isEmpty, s"Blast hits: ${hits}")
  }

}

enum TagTestResult(testName: String) {
  def isPass: Boolean = this match { case passed(_) => true ; case _ => false }
  def isFailure: Boolean = this match { case failed(_, _) => true ; case _ => false }

  case passed(testName: String) extends TagTestResult(testName)
  case failed(testName: String, explanation: String) extends TagTestResult(testName)
}

object TagTestResult {
  def apply(testName: String, test: Boolean, explanation: => String): TagTestResult = 
    if(test) TagTestResult.passed(testName)
    else TagTestResult.failed(testName, explanation)
}

type TagTestResults = List[TagTestResult]
def (rs: TagTestResults) isPass: Boolean = rs.forall(_.isPass)
inline def (rs: TagTestResults) isFailure: Boolean = !rs.isPass

case class TestMatrix(
  passing:      Map[Dna, Boolean],
  singleTests:  Map[Dna, TagTestResults],
  pairTests:    Map[(Dna, Dna), TagTestResults])

object TestMatrix {
  def empty: TestMatrix = TestMatrix(Map.empty, Map.empty, Map.empty)
}

case class MatrixBuilder(
  singleTest: Dna => TagTestResults,
  pairTest: (Dna, Dna) => TagTestResults)
{
  def (matrix: TestMatrix) appendTag(tag: Dna): TestMatrix = 
    matrix.passing.keys.foldLeft(matrix.applySingleTest(tag))((m, t2) => 
      if(t2 == tag) m                     // don't pair test against yourself
      else if(!m.passing(t2)) m  // skip pair testing against already failed tags
      else m.applyPairTest(tag, t2))      // ok then, test this pair
   

  private 
  def (matrix: TestMatrix) updatePassing(tag: Dna, passing: Boolean): TestMatrix = matrix.copy(
    passing = matrix.passing +
      ((tag, matrix.passing.getOrElse(tag, true) && passing))
  )

  private 
  def (matrix: TestMatrix) applySingleTest(tag: Dna): TestMatrix = {
    val testedTag = singleTest(tag)

    matrix.updatePassing(tag, testedTag.isPass).copy(
      singleTests = matrix.singleTests + ((tag -> testedTag)))
  }

  private 
  def (matrix: TestMatrix) applyPairTest(tag1: Dna, tag2: Dna): TestMatrix = {
    val testedPair = pairTest(tag1, tag2)
    val passed = testedPair.isPass

    matrix.updatePassing(tag1, passed).updatePassing(tag2, passed).copy(
      pairTests = matrix.pairTests + (((tag1, tag2), testedPair))
    )
  }
}