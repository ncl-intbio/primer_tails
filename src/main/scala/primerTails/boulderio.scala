package primerTails


@FunctionalInterface
trait BoulderValue[V] {
  def valueString(v: V): String
}
object BoulderValue {
  implied for BoulderValue[String] = identity
  implied for BoulderValue[Int] = _.toString
  implied for BoulderValue[Double] = _.toString
}


trait Boulder[R[_]] {
  type Boulder
  type Stone
  type Property

  def boulder(stone: R[Stone]*): R[Boulder]
  def stone(properties: R[Property]*): R[Stone]
  def property(name: String, value: String): R[Property]

  def (name: String) := [V](value: V) given (V: BoulderValue[V]) = property(name, V.valueString(value))

}

object Boulder {

  def parse[R[_]](text: String) given (B: Boulder[R]): R[B.Boulder] = {
    import B._

    // fixme: not tail recursive
    def eachStone(ls: List[String]): List[R[Stone]] = ls match {
      case Nil => Nil
      case lines =>
        val (stone1, rest) = lines.span(_.indexOf('=') > 1)
        val props = stone1.map { s =>
          val Array(name, value) = s.split('=')
          B.property(name, value) 
        }
        B.stone(props :_*) :: eachStone(rest.tail)
    }

    val allLines = text.split('\n').to[List]
    B.boulder(eachStone(allLines) :_*)
  }

  implied StringBoulder for Boulder[[?] => String] {
    type Boulder = String
    type Stone = String
    type Property = String

    def boulder(stones: String*): String = stones.mkString
    def stone(properties: String*): String = properties.mkString("", "\n", "\n=\n")
    def property(name: String, value: String): String = s"${name.trim}=${value.trim}"
  }

  implied MapBoulder for Boulder[[A] => A] {
    type Boulder = List[Stone]
    type Stone = Map[String, String]
    type Property = (String, String)

    def boulder(stones: Stone*): Boulder = stones.to[List]
    def stone(properties: Property*): Stone = properties.toMap
    def property(name: String, value: String): (String, String) = (name, value)
  }
}

