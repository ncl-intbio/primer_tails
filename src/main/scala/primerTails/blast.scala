package primerTails

case class BlastNRecord(
  query: String,
  subject: String,
  pc_identity: Double,
  alignmentLength: Int,
  mismatches: Int,
  gapOpens: Int,
  queryStart: Int,
  queryEnd: Int,
  subjectStart: Int,
  subjectEnd: Int,
  evalue: Double,
  bitScore: Double)

object Blast {
  def parseTabular(blastTab: String): List[BlastNRecord] = for {
    line <- augmentString(blastTab).lines.to[List]
      if(!line.startsWith("#"))
  } yield {
    val Array(
      query,
      subject,
      pc_identity,
      alignmentLength,
      mismatches,
      gapOpens,
      queryStart,
      queryEnd,
      subjectStart,
      subjectEnd,
      evalue,
      bitScore) = line.split("\t")

    BlastNRecord(
      query           = query,
      subject         = subject,
      pc_identity     = pc_identity.toDouble,
      alignmentLength = alignmentLength.toInt,
      mismatches      = mismatches.toInt,
      gapOpens        = gapOpens.toInt,
      queryStart      = queryStart.toInt,
      queryEnd        = queryEnd.toInt,
      subjectStart    = subjectStart.toInt,
      subjectEnd      = subjectEnd.toInt,
      evalue          = evalue.toDouble,
      bitScore        = bitScore.toDouble)
  }

  def byQuery(records: List[BlastNRecord]): Map[String, List[BlastNRecord]] =
    records.groupBy(_.query)
    
}
