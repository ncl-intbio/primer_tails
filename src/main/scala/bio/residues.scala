package bio

import scala.language.implicitConversions
import cats.implicits._
import ct._
import implied ct._

/** Residues in bioplymers.
 *
 * Cannonically, the biopolymers are DNA, RNA and Protein. However, there are a range of synthetic and extended biopolymers.
 */
trait Residue[R] {
  def residueChar(r: R): Char
  def toResidue(c: Char): Err[R]
}

implied RenderResidue[R] given (R: Residue[R]) for Rendering[R] = Rendering.from(R.residueChar)

/** Bases are residues in biopolymers that can additionally complement one-another.
 */
trait Base[B] {
  def complementary(r: B): B
}

// begin syntax
def (c: Char) toResidue[R]      given Residue[R]: Err[R] = the[Residue[R]].toResidue(c)
def (b: B)    complementary[B]  given Base[B]:    B      = the[Base[B]].complementary(b)
// end syntax




/** DNA Nucleotides.
 *
 * This is the core alphabet of DNA nucleotides, without ambiguity.
 * It is typically used to represent both DNA and RNA, although technically RNA uses U(racil) in place of T(hymine).
 */ 
enum Nucleotide {
  case A
  case G
  case C
  case T
}

object Nucleotide {
  implied for Uniform[Nucleotide] = enumValues.uniform

  implied for Residue[Nucleotide] {
    import Nucleotide._

    def toResidue(c: Char): Err[Nucleotide] = c match {
      case 'a' | 'A' => A.success
      case 'c' | 'C' => C.success
      case 'g' | 'G' => G.success
      case 't' | 'T' => T.success
      case _ => s"Unable to parse '$c' as a nucleotide".error
    }

    def residueChar(n: Nucleotide): Char = n match {
      case A => 'a'
      case G => 'g'
      case C => 'c'
      case T => 't'
    }
  }

  implied for Base[Nucleotide] {
    def complementary(n: Nucleotide): Nucleotide = n match {
      case A => T
      case G => C
      case C => G
      case T => A
    }
  }

}



/** DNA Nucleotides with ambiguity codes.
 *
 * This is the core alphabet of DNA nucleotides extended by codes representing ambiguous nucleotides.
 * For example, `N` represents all four bases.
 */ 
enum IupacNucleotide(nucleotides: List[Nucleotide]) {
  case A    extends IupacNucleotide(Nucleotide.A                                                  ::Nil)
  case G    extends IupacNucleotide(                Nucleotide.G                                  ::Nil)
  case C    extends IupacNucleotide(                                Nucleotide.C                  ::Nil)
  case T    extends IupacNucleotide(                                                Nucleotide.T  ::Nil)
  case K    extends IupacNucleotide(                Nucleotide.G                  ::Nucleotide.T  ::Nil)
  case M    extends IupacNucleotide(Nucleotide.A                  ::Nucleotide.C                  ::Nil)
  case R    extends IupacNucleotide(Nucleotide.A  ::Nucleotide.G                                  ::Nil)
  case Y    extends IupacNucleotide(                                Nucleotide.C  ::Nucleotide.T  ::Nil)
  case S    extends IupacNucleotide(                Nucleotide.G  ::Nucleotide.C                  ::Nil)
  case W    extends IupacNucleotide(Nucleotide.A                                  ::Nucleotide.T  ::Nil)
  case B    extends IupacNucleotide(                Nucleotide.G  ::Nucleotide.C  ::Nucleotide.T  ::Nil)
  case V    extends IupacNucleotide(Nucleotide.A  ::Nucleotide.G  ::Nucleotide.C                  ::Nil)
  case H    extends IupacNucleotide(Nucleotide.A                  ::Nucleotide.C  ::Nucleotide.T  ::Nil)
  case D    extends IupacNucleotide(Nucleotide.A  ::Nucleotide.G                  ::Nucleotide.T  ::Nil)
  case X    extends IupacNucleotide(Nucleotide.A  ::Nucleotide.G  ::Nucleotide.C  ::Nucleotide.T  ::Nil)
  case N    extends IupacNucleotide(Nucleotide.A  ::Nucleotide.G  ::Nucleotide.C  ::Nucleotide.T  ::Nil)
  case `.`  extends IupacNucleotide(                                                                Nil)
}

object IupacNucleotide {

  implied for Residue[IupacNucleotide] {
    import IupacNucleotide._

    def toResidue(c: Char): Err[IupacNucleotide] = c match {
      case 'a' | 'A' => A.success
      case 'c' | 'C' => C.success
      case 'g' | 'G' => G.success
      case 't' | 'T' => T.success
      case 'k' | 'K' => K.success
      case 'm' | 'M' => M.success
      case 'r' | 'R' => R.success
      case 'y' | 'Y' => Y.success
      case 's' | 'S' => S.success
      case 'w' | 'W' => W.success
      case 'b' | 'B' => B.success
      case 'v' | 'V' => V.success
      case 'h' | 'H' => H.success
      case 'd' | 'D' => D.success
      case 'x' | 'X' => X.success
      case 'n' | 'N' => N.success
      case '.'       => `.`.success
      case _ => s"Unable to parse '$c' as an IUPAC nucleotide".error
    }

    def residueChar(n: IupacNucleotide): Char = n match {
      case A => 'a'
      case G => 'g'
      case C => 'c'
      case T => 't'
      case K => 'k'
      case M => 'm'
      case R => 'r'
      case Y => 'y'
      case S => 's'
      case W => 'w'
      case B => 'b'
      case V => 'v'
      case H => 'h'
      case D => 'd'
      case X => 'x'
      case N => 'n'
      case `.` => '.'
    }
  }

  implied for Base[IupacNucleotide] {
    def complementary(n: IupacNucleotide): IupacNucleotide = n match {
      case A => T
      case G => C
      case C => G
      case T => A
      case K => M
      case M => K
      case R => Y
      case Y => R
      case S => S
      case W => W
      case B => V
      case V => B
      case H => D
      case D => H
      case X => X
      case N => N
      case `.` => `.`
    }
  }
}


/** Amino acid residues.
 *
 * Unlike DNA, the amino-acid alphabet by convention always includes ambiguity codes.
 */
enum AminoAcid(val oneLetter: Char, val threeLetter: String, val name: String) {
  case A extends AminoAcid('A', "Ala", "Alanine")
  case B extends AminoAcid('B', "Asx", "Aspartic Acid or Asparagine")
  case C extends AminoAcid('C', "Cys", "Cysteine")
  case D extends AminoAcid('D', "Asp", "Aspartic Acid")
  case E extends AminoAcid('E', "Glu", "Glutamic Acid")
  case F extends AminoAcid('F', "Phe", "Phenylalanine")
  case G extends AminoAcid('G', "Gly", "Glycine")
  case H extends AminoAcid('H', "His", "Histidine")
  case I extends AminoAcid('I', "Ile", "Isoleucine")
  case J extends AminoAcid('J', "Xle", "Leucine or Isoleucine")
  case K extends AminoAcid('K', "Lys", "Lysine")
  case L extends AminoAcid('L', "Leu", "Leucine")
  case M extends AminoAcid('M', "Met", "Methionione")
  case O extends AminoAcid('O', "Pyl", "Pyrrolysine")
  case P extends AminoAcid('P', "Pro", "Proline")
  case Q extends AminoAcid('Q', "Gln", "Glutamine")
  case R extends AminoAcid('R', "Arg", "Arginine")
  case S extends AminoAcid('S', "Ser", "Serine")
  case T extends AminoAcid('T', "Thy", "Threonine")
  case U extends AminoAcid('U', "Sec", "Selenocysteine")
  case V extends AminoAcid('V', "Val", "Valine")
  case W extends AminoAcid('W', "Trp", "Tryptophan")
  case X extends AminoAcid('X', "Xaa", "Unspecified or unknown")
  case Y extends AminoAcid('Y', "Tyr", "Tyrosine")
  case Z extends AminoAcid('Z', "Glx", "Glutamic Acid or Glutamine")
}

object AminoAcid {
  import AminoAcid._

  implied for Residue[AminoAcid] {
    def residueChar(aa: AminoAcid): Char = 
      aa.oneLetter

    def toResidue(c: Char): Err[AminoAcid] =
      AminoAcid.enumValues.find(_.oneLetter == c)
        .fold("Unable to parse $c as an amino acid".error[AminoAcid])(_.success)
  }
}