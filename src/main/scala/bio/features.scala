package bio

import scala.language.implicitConversions

import cats._
import cats.implicits._

/** A region of a biopolymer, inclusive of start and of end.
 *
 * length = start + end - 1
 */
case class Region(start: Int, end: Int)

case class Frequencies[T](counts: Map[T, Int])

def (p: P) residueFrequencies[P, R](windowWidth: Int) given Biopolymer[P], BiopolymerResidues[P, R]: List[(Region, Frequencies[R])] = for {
  i <- (1 to (p.length - windowWidth)).to[List]
} yield {
  val cs = for {
    j <- (0 until windowWidth).to[List]
  } yield Map((p residue (i+j), 1))
  (Region(i, i + windowWidth - 1), Frequencies(cs.combineAll))
}