package bio

import scala.language.implicitConversions

import cats._
import cats.implicits._
import ct._
import implied ct._

// begin syntax
inline def (str: String) toBiopolymer[P] given Biopolymer[P]: Err[P] =
  the[Biopolymer[P]].toBiopolymer(str)

inline def (str: String) toProtein  = str.toBiopolymer[Protein]
inline def (str: String) toDna      = str.toBiopolymer[Dna]
inline def (str: String) toIupacDna = str.toBiopolymer[IupacDna]

inline def (p: P) length[P] given Biopolymer[P]: Int =
  the[Biopolymer[P]].length(p)

inline def (p: P) subregion[P] (start: Int, end: Int) given Biopolymer[P]: P =
  the[Biopolymer[P]].subregion(p, start, end)

inline def (toLeft: P) join[P] (toRight: P) given Biopolymer[P]: P =
  the[Biopolymer[P]].join(toLeft, toRight)

inline def (p: P) reverse[P, Q] given BasePolymerTransforms[P, Q]: Q =
  the[BasePolymerTransforms[P, Q]].reverse(p)

inline def (p: P) complement[P, Q] given BasePolymerTransforms[P, Q]: Q =
  the[BasePolymerTransforms[P, Q]].complement(p)

inline def (p: P) reverseComplement[P, Q] given BasePolymerTransforms[P, Q]: P =
  the[BasePolymerTransforms[P, Q]].reverseComplement(p)

inline def (p: P) residue[P, R](at: Int) given BiopolymerResidues[P, R]: R =
  the[BiopolymerResidues[P, R]].residue(p, at)
// end syntax


/** A boxed form of a polymer.
 *
 * @tparam W  the wrapping type
 * @tparam U  the underlying wrapped type
 */
trait WrappedPolymer[W, U] {
  def unwrap(w: W): U
  def wrap(u: U): W
}

object WrappedPolymer {
  implied RenderWrapped[W, S <: Iterable, R] given (W: WrappedPolymer[W, S[R]], R: Residue[R]) for Rendering[W] =
    w => Rendering.all(W.unwrap(w))
}


/** A bio-polymer is a polymer of biochemical residues.
 *
 * Biopolymers are sequences of residues.
 * Biologists count from 1 to length, inclusive of both indexes, so all index-related operations use this system.
 *
 * @tparam P  the underlying type to treat as a bioplymer
 */
trait Biopolymer[P] {
  def toBiopolymer(ns: String): Err[P]

  def length(polymer: P): Int
  def subregion(polymer: P, start: Int, end: Int): P
  def join(toLeft: P, toRight: P): P
}

object Biopolymer {
  /** Any scala `Seq` of residues can be treated as a biopolymer. */
  implied SeqBiopolymer[S <: Seq, R]
    given (R: Residue[R], T: Traverse[S], A: Applicative[S], M: MonoidK[S]) for Biopolymer[S[R]] {

    def toBiopolymer(ns: String): Err[S[R]] =
      ns.to[List].foldMap(A.point)(M.algebra).traverse(R.toResidue)
      // the string has to be converted to a concrete type to let us convert it into S[Char]
      
    def length(polymer: S[R]): Int =
      polymer.length

    def render(polymer: S[R]): String =
      polymer.foldMap(r => R.residueChar(r).toString)

    def subregion(polymer: S[R], start: Int, end: Int): S[R] =
      polymer.slice(start-1, end).asInstanceOf[S[R]] // todo: not sure if this is safe!!!

    def join(toLeft: S[R], toRight: S[R]): S[R] =
      M.algebra.combine(toLeft, toRight)
  }

  implied Wrapped[W, U] given (W: WrappedPolymer[W, U], U: Biopolymer[U]) for Biopolymer[W] {
    def toBiopolymer(ns: String): Err[W] = U.toBiopolymer(ns).map(W.wrap)
    def length(p: W): Int = U.length(W.unwrap(p))
    def subregion(p: W, start: Int, end: Int): W = W.wrap(U.subregion(W.unwrap(p), start, end))
    def join(toLeft: W, toRight: W): W = W.wrap(U.join(W.unwrap(toLeft), W.unwrap(toRight)))
  }
}


/** Transformations between biopolymers of bases. */
trait BasePolymerTransforms[P, Q] {
  def reverse(polymer: P): Q
  def complement(polymer: P): Q
  def reverseComplement(polymer: P): P
}

object BasePolymerTransforms {

  private
  def (fa: F[A]) reverseF[F[_], A] given (F: Foldable[F], P: Applicative[F], A: Monoid[F[A]]): F[A] =
    F.foldLeft(fa, A.empty)((b, a) => A.combine(a.pure, b))

  implied SeqTransforms[S[_], R] given (S: Foldable[S], M: Monoid[S[R]], A: Applicative[S], B: Base[R]) for BasePolymerTransforms[S[R], S[R]] {
    def reverse(polymer: S[R]): S[R] = polymer.reverseF
    def complement(polymer: S[R]): S[R] = polymer.map(the[Base[R]].complementary)
    def reverseComplement(polymer: S[R]): S[R] = polymer.reverseF.map(the[Base[R]].complementary)
  }

  implied Wrapped[P, Q, PW, QW] given (
    P: WrappedPolymer[P, PW],
    Q: WrappedPolymer[Q, QW],
    PQ: BasePolymerTransforms[PW, QW]) for BasePolymerTransforms[P, Q] {

    def reverse(p: P): Q = 
      Q.wrap(
        PQ.reverse(
          P.unwrap(p)))

    def complement(p: P): Q =
      Q.wrap(
        PQ.complement(
          P.unwrap(p)))
    
    def reverseComplement(p: P): P =
      P.wrap(
        PQ.reverseComplement(
          P.unwrap(p)))
  }
}


/** Biopolymer with access to the underlying residues. */
trait BiopolymerResidues[P, R] {
  def residue(polymer: P, at: Int): R
}

object BiopolymerResidues {
  implied SeqResidues[S <: Seq, R] for BiopolymerResidues[S[R], R] {
    def residue(p: S[R], at: Int): R = p(at - 1)
  }

  implied Wrapped[P, Q, R] given (P: WrappedPolymer[P, Q], Q: BiopolymerResidues[Q, R]) for BiopolymerResidues[P, R] {
    def residue(p: P, at: Int): R = 
      Q.residue(
        P.unwrap(p),
        at)
  }
}


/** A protein, as a polymer of amino acids. */
opaque type Protein = List[AminoAcid]

object Protein {
  implied Wrapper for WrappedPolymer[Protein, List[AminoAcid]] {
    def wrap(a: List[AminoAcid]): Protein = a
    def unwrap(p: Protein): List[AminoAcid] = p
  }

  // fixme: I had to supply the `given` clause to prevent an infinite loop !!!
  implied for Biopolymer[Protein] = Biopolymer.Wrapped[Protein, List[AminoAcid]] given (Protein.Wrapper, Biopolymer.SeqBiopolymer[List, AminoAcid])
}


/** DNA.
 *
 * This is represented in the 5' to 3' direction.
 * This is the forward direction, and in a double-stranded molecule represents the top strand.
 */
opaque type Dna = List[Nucleotide]

object Dna {
  def random(l: Int) given (N: Uniform[Nucleotide]): Rand[Dna] = Rand {
    r => Wrapper.wrap(N.rand.replicate(l).sample(r))
  }

  implied Wrapper for WrappedPolymer[Dna, List[Nucleotide]] {
    def wrap(ns: List[Nucleotide]): Dna = ns
    def unwrap(d: Dna): List[Nucleotide] = d
  }

  // fixme: I had to supply the `given` clause to prevent an infinite loop !!!
  implied for Biopolymer[Dna] = Biopolymer.Wrapped[Dna, List[Nucleotide]] given (Dna.Wrapper, Biopolymer.SeqBiopolymer[List, Nucleotide])

  implied for BasePolymerTransforms[Dna, Dna_ReverseStrand] =
    BasePolymerTransforms.Wrapped
  
  implied for Rendering[Dna] = WrappedPolymer.RenderWrapped
}


/** Dna Reverse Strand
 *
 * This is represented in the 3' to 5' direction.
 * This is the backwards direction, and in a double-stranded molecule represents the bottom strand.
 */
opaque type Dna_ReverseStrand = List[Nucleotide]

object Dna_ReverseStrand {
  implied Wrapper for WrappedPolymer[Dna_ReverseStrand, List[Nucleotide]] {
    def wrap(ns: List[Nucleotide]): Dna_ReverseStrand = ns
    def unwrap(d: Dna_ReverseStrand): List[Nucleotide] = d
  }

  // fixme: I had to supply the `given` clause to prevent an infinite loop !!!
  implied for Biopolymer[Dna_ReverseStrand] = Biopolymer.Wrapped[Dna_ReverseStrand, List[Nucleotide]] given (Dna_ReverseStrand.Wrapper, Biopolymer.SeqBiopolymer[List, Nucleotide])

  implied for BasePolymerTransforms[Dna_ReverseStrand, Dna] =
    BasePolymerTransforms.Wrapped

  implied for Rendering[Dna_ReverseStrand] = WrappedPolymer.RenderWrapped
}


/** Dna, alowing the full range of IUPAC ambiguity codes. */
opaque type IupacDna = List[IupacNucleotide]
object IupacDna {
  implied Wrapper for WrappedPolymer[IupacDna, List[IupacNucleotide]] {
    def wrap(ns: List[IupacNucleotide]): IupacDna = ns
    def unwrap(d: IupacDna): List[IupacNucleotide] = d
  }

  // fixme: I had to supply the `given` clause to prevent an infinite loop !!!
  implied for Biopolymer[IupacDna] = Biopolymer.Wrapped[IupacDna, List[IupacNucleotide]] given (IupacDna.Wrapper, Biopolymer.SeqBiopolymer[List, IupacNucleotide])

  implied for BasePolymerTransforms[IupacDna, IupacDna_ReverseStrand] =
    BasePolymerTransforms.Wrapped

  implied for Rendering[IupacDna] = WrappedPolymer.RenderWrapped
}


/** Dna reverse strand, allowing the full range of IUPAC ambiguity codes. */
opaque type IupacDna_ReverseStrand = List[IupacNucleotide]
object IupacDna_ReverseStrand {
  implied Wrapper for WrappedPolymer[IupacDna_ReverseStrand, List[IupacNucleotide]] {
    def wrap(ns: List[IupacNucleotide]): IupacDna_ReverseStrand = ns
    def unwrap(d: IupacDna_ReverseStrand): List[IupacNucleotide] = d
  }

  // fixme: I had to supply the `given` clause to prevent an infinite loop !!!
  implied for Biopolymer[IupacDna_ReverseStrand] = Biopolymer.Wrapped[IupacDna_ReverseStrand, List[IupacNucleotide]] given (IupacDna_ReverseStrand.Wrapper, Biopolymer.SeqBiopolymer[List, IupacNucleotide])

  implied for BasePolymerTransforms[IupacDna_ReverseStrand, IupacDna_ReverseStrand] =
    BasePolymerTransforms.Wrapped

  implied for Rendering[IupacDna_ReverseStrand] = WrappedPolymer.RenderWrapped
}
