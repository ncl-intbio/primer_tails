package bio

import scala.language.implicitConversions


import cats._
import cats.implicits._

import ct._
import implied ct._

case class Fasta[De, Sq](description: De, sequence: Sq) {
  def mapDesc[Ed](f: De=>Ed): Fasta[Ed, Sq] = copy(description = f(description), sequence)
  def mapSeq[Qs](f: Sq=>Qs): Fasta[De, Qs] = copy(sequence = f(sequence))
}

object Fasta {
  case class Description(id: Option[String], descr: Option[String])

  implied for Rendering[Description] = d =>
   ">".toRender |+| " ".toRender |+| d.id.toRender |+|
     d.descr.fold("".toRender)(e => " ".toRender |+| e.toRender) |+| "\n".toRender

  implied RenderFasta[De, Sq] given (De: Rendering[De], Sq: Rendering[Sq]) for Rendering[Fasta[De, Sq]] =
   fa => De.toRender(fa.description) |+| Sq.toRender(fa.sequence) |+| "\n".toRender

  def parse(lines: List[String]): List[RawFasta] = lines match {
    case h::t if h.trim.isEmpty =>
      parse(t)
    case h::t if h.startsWith(">") =>
      val (s, ll) = t.span(l => !l.startsWith(">"))
      Fasta(h, s)::parse(ll)
    case Nil =>
      Nil
  }

  def parse(text: String): List[RawFasta] =
    parse(text.split("\n").to[List])
  
  val Descr = ">(\\S*)\\s*(.*)".r
  def parseDescr(descrLine: String): Description = descrLine match {
    case Descr(id, de) => Description(
      if(id.isEmpty) None else Some(id),
      if(de.isEmpty) None else Some(de)
    )
  }

  def parseSeqToDna(seqLines: List[String]): Err[Dna] =
    seqLines.map(_.replaceAll("\\s", "")).mkString("").toDna

  def parseSeqToDnaIupac(seqLines: List[String]): Err[IupacDna] =
    seqLines.map(_.replaceAll("\\s", "")).mkString("").toIupacDna
}


type RawFasta = Fasta[String, List[String]]
type FastaDna = Fasta[Fasta.Description, Dna]
type FastaIupacDna = Fasta[Fasta.Description, IupacDna]


def (fa: RawFasta) toDna: Err[FastaDna] =
  Fasta.parseSeqToDna(fa.sequence) map { 
    s => fa.mapDesc(Fasta.parseDescr).copy(sequence = s) }

def (fa: RawFasta) toIupacDna: Err[FastaIupacDna] =
  Fasta.parseSeqToDnaIupac(fa.sequence) map { 
    s => fa.mapDesc(Fasta.parseDescr).copy(sequence = s) }
