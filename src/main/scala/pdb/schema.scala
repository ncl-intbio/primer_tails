package pdb

// Primitives for parsing by-line, column-based records from file formats like PDB



/** A full file (multiple lines) in a PDB-like file.
  *
  * Each file is composed of multiple row record types.
  * These record types may be repeated, or optional, or in specific orders.
  */
trait PdbFile[F[_]] {
  
//  def file[LS, T](lines: LS) given FileBuilder[LS, T]: F[T] = the[FileBuilder[LS, T]].build(lines)

  trait FileBuilder[LS, T] {
    def buildFile(lines: LS): F[T]
  }
}



/** A record (line) in a PDB-like file.
  *
  * Each record is a name followed by zero or more columns.
  */
trait PdbRecord[R[_]] {
  type C[_]

  def record(charStart: Int, charEnd: Int, name: String): WithColumns

  trait WithColumns {
    def withColumns[CS, T](columns: CS) given ColumnBuilder[CS, T]: R[T]
  }

  trait ColumnBuilder[CS, T] {
    def build[CS, T](cs: CS): R[T]
  }
}



/** Definition of record name.
  *
  * Record names define records. Each record name is associated with exactly one type of record line.
  */
@FunctionalInterface
trait PdbRecordName[N] {
  def recordName(charStart: Int, charEnd: Int, name: String): N
}

/** Definition of a data-carying column.
  *
  * Data columns segment out the chars in a range and then parse them to a value.
  */
@FunctionalInterface
trait PdbColumn[C[_], T] {
  def col[T](charStart: Int, charEnd: Int, field: String, definition: String): C[T]

  def (ts: Tuple4[Int, Int, String, String]) col[T]: C[T] = col[T](ts._1, ts._2, ts._3, ts._4)
}
