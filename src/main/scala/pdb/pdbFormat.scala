package pdb
/*
object Pdb {
  enum PList {
    case End
    case Cons [H, T <: PList](h: LineParser[H], t: T)
  }

  import PList._

  def (h: LineParser[H]) +: [H, T <: PList] (t: T) = Cons(h, t)

  def all =
     Helix.parser +:
     Sheet.parser +:
     Turn.parser +:
     Model.parser +:
     Atom.parser +: End
  
  def p[L, R](lhs: LineParser[L], rhs: LineParser[R]): LineParser[Seq[L], R] = {
    def x(lines: List[CharSequence]) = lines match {
      case Nil   => Eof
      case l::ls => lhs.parse(l) match {
        case Accept(lv) => recurse(ls, lv)
        case Reject => rhs(lines) match {
          case Accept(rv) => recurse(ls, rv)
          case Reject => x(ls)
        }
      }
    }
  }
}
*/
