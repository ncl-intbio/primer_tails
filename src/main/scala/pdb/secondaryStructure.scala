package pdb

// As described in http://www.wwpdb.org/documentation/file-format-content/format23/sect5.html
/*
case class Helix(
  serNum: Int,
  helixId: LString_3,
  initResName: ResidueName,
  initChainID: Char,
  initSeqNum: Int,
  initICode: AChar,
  endResName: ResidueName,
  endChainID: Char,
  endSeqNum: Int,
  endICode: AChar,
  helixClass: HelixClass,
  comment: String,
  length: Int
  )

object Helix {

  type Record = (((((((((((((String, Int), String), String), Char), Int), Char), String), Char), Int), Char), pdb.HelixClass), String), Int)

  def apply[R[_], N, C[_]] given PdbRecord[R, N, C],
                                 PdbRecordName[N],
                                 PdbColumn[C, Int],
                                 PdbColumn[C, Char],
                                 PdbColumn[C, String],
                                 PdbColumn[C, HelixClass]: R[Record] = {
    val R = the[PdbRecord[R, N, C]]
    import R._
    record(1, 6, "HELIX ") withColumns (
      col[Int](8, 10, "serNum", "Serial number of the helix. This starts at 1 and increases incrementally.") *:
      col[LString_3](12, 14, "helixID", "Helix identifier. In addition to a serial number, each helix is to a serial number, each helix is helix identifier.") *:
      col[ResidueName](16, 18, "initResName", "Name of the initial residue.") *:
      col[Char](20, 20, "initChainID", "Chain identifier for the chain containing this helix.") *:
      col[Int](22, 25, "initSeqNum", "Sequence number of the initial residue.") *:
      col[AChar](26, 26, "initICode", "Insertion code of the initial residue.") *:
      col[ResidueName](28, 30, "endResName", "Name of the terminal residue of the helix.") *:
      col[Char](32, 32, "endChainID", "Chain identifier for the chain containing this helix.") *:
      col[Int](34, 37, "endSeqNum", "Sequence number of the terminal residue.") *:
      col[AChar](38, 38, "endICode", "Insertion  code of the terminal residue.") *:
      col[HelixClass](39, 40, "helixClass", "Helix class") *:
      col[String](41, 70, "comment", "Comment about this helix.") *:
      col[Int](72, 76, "length", "Length of this helix.") *:
      ()
    )
  }

  def parser = Helix.apply[LineParser, ParseCS[String], ParseCS]
}

enum HelixClass {
  case Unk
  case `Right-handed alpha`
  case `Right-handed omega`
  case `Right-handed pi`
  case `Right-handed gamma`
  case `Right-handed 310`
  case `Left-handed alpha`
  case `Left-handed omega`
  case `Left-handed gamma`
  case `27 ribbon/helix`
  case `Polyproline`
}

implied for ParseCS[HelixClass] =
 the[ParseCS[Int]].
   withDefault(1).
   mapOption(HelixClass.enumValue.get)


object Sheet {

  def apply[R[_], N, C[_]] given PdbRecord[R, N, C],
                                 PdbRecordName[N],
                                 PdbColumn[C, Int],
                                 PdbColumn[C, Char],
                                 PdbColumn[C, String],
                                 PdbColumn[C, SheetStrandSense] = {
    val R = the[PdbRecord[R, N, C]]
    import R._
    record(1, 6, "SHEET ") withColumns (
      col[Int](8, 10, "strand", "Strand number which starts at 1 for each strand within a sheet and increases by 1.") *:
      col[LString_3](12, 14, "sheetID", "Sheet identifier.") *:
      col[Int](15, 16, "numStrands", "Number of strands in sheet.") *:
      col[ResidueName](18, 20, "intResName", "Residue name of initial residue.") *:
      col[Char](22, 22, "initChainID", "Chain identifier of initial residue in strand.") *:
      col[Int](23, 26, "initSeqNum", "Sequence number of initial residue in strand.") *:
      col[AChar](27, 27, "initICode", "Insertion code of initial residue in strand.") *:
      col[ResidueName](29, 31, "endResName", "Residue name of terminal residue.") *:
      col[Char](33, 33, "endChainID", "Chain identifier of terminal residue.") *:
      col[Int](34, 37, "endSeqNum", "Sequence number of terminal residue.") *:
      col[AChar](38, 38, "endICode", "Insertion code of terminal residue.") *:
      col[SheetStrandSense](39, 40, "sense", "Sense of strand with respect to previous strand in the sheet. 0 if first strand, 1 if parallel, -1 if antiparallel") *:
      col[Atom](42, 45, "curAtom", "Registration. Atom name in current strand.") *:
      col[ResidueName](46, 48, "curResName", "Registration. Residue name in current strand.") *:
      col[Char](50, 50, "curChainId", "Registration. Chain identifier in current strand.") *:
      col[Int](51, 54, "curResSeq", "Registration. Residue sequence number in current strand.") *:
      col[AChar](55, 55, "curICode", "Registration. Insertion code in current strand.") *:
      col[Atom](57, 60, "prevAtom", "Registration. Atom name in previous strand") *:
      col[ResidueName](61, 63, "prevResName", "Registration. Residue name in previous strand.") *:
      col[Char](65, 65, "prevChainId", "Registration. Chain identifier in previous strand.") *:
      col[Int](66, 69, "prevResSeq", "Registration. Residue sequence number in previous strand") *:
      col[AChar](70, 70, "prevICode", "Registration. Insertion code in previous strand.") *:
      ()
    )
  }

  def parser = apply[LineParser, ParseCS[String], ParseCS]
}

enum SheetStrandSense(constant: Int) {
  case `first strand` extends SheetStrandSense(0)
  case parallel extends SheetStrandSense(+1)
  case antiparallel extends SheetStrandSense(-1)
}

implied for ParseCS[SheetStrandSense] = the[ParseCS[Int]].mapOption {
  case  0 => Some(SheetStrandSense.`first strand`)
  case  1 => Some(SheetStrandSense.parallel)
  case -1 => Some(SheetStrandSense.antiparallel)
  case _ => None
}
  


object Turn {

  def apply[R[_], N, C[_]] given PdbRecord[R, N, C],
                                 PdbRecordName[N],
                                 PdbColumn[C, Int],
                                 PdbColumn[C, Char],
                                 PdbColumn[C, String] = {
    val R = the[PdbRecord[R, N, C]]
    import R._
    record(1, 6, "TURN ") withColumns (
      col[Int](8, 10, "seq", "Turn number; starts with 1 and increments by one.") *:
      col[LString_3](12, 14, "turnId", "Turn identifier") *:
      col[ResidueName](16, 18, "initResName", "Residue name of initial residue in turn.") *:
      col[Char](20, 20, "initChainId", "Chain identifier for the chain containing this turn.") *:
      col[Int](21, 24, "initSeqNum", "Sequence number of initial residue in turn. ") *:
      col[AChar](25, 25, "initICode", "Insertion code of initial residue in turn.") *:
      col[ResidueName](27, 29, "endResName", "Residue name of terminal residue of turn.") *:
      col[Char](31, 31, "endChainId", "Chain identifier for the chain containing this turn.") *:
      col[Int](32, 35, "endSeqNum", "Sequence number of terminal residue of turn.") *:
      col[AChar](36, 36, "endICode", "Insertion code of terminal residue of turn.") *:
      col[String](41, 70, "comment", "Associated comment") *:
      ()
    )
  }

  def parser = apply[LineParser, ParseCS[String], ParseCS]

}
*/
