package pdb

// As described in http://www.wwpdb.org/documentation/file-format-content/format23/sect9.html

case class Model(serial: Int)

object Model {

  type Record = (String, Int)

  def line[R[_], C[_]] given PdbRecord[R], PdbColumn[C, Int]: R[Record] = {
    val R = the[PdbRecord[R]]
    import R._
    record(1, 6, "MODEL ") withColumns (
      (11, 14, "serial", "Model serial number.").col[Int] *:
      ()
    )
  }

  //def parser = line[LineParser, ParseCS[String], ParseCS].map(_._2).map(Model.apply)
}

/*

object Atom {
  def apply[R[_], N, C[_]] given PdbRecord[R, N, C],
                                 PdbRecordName[N],
                                 PdbColumn[C, Int],
                                 PdbColumn[C, Char],
                                 PdbColumn[C, String],
                                 PdbColumn[C, Double] = {
    val R = the[PdbRecord[R, N, C]]
    import R._
    record(1, 6, "ATOM  ") withColumns (
      col[Int](7, 11, "serial", "Atom serial number.") *:
      col[Atom](13, 16, "name", "Atom name.") *:
      col[Char](17, 17, "altLoc", "Alternative location indicator.") *:
      col[ResidueName](18, 20, "resName", "Residue name.") *:
      col[Char](22, 22, "chainID", "Chain identifier.") *:
      col[Int](23, 26, "resSeq", "Residue sequence number.") *:
      col[AChar](27, 27, "iCode", "Code for insertion of residues.") *:
      col[Real_8_3](31, 38, "x", "Orthogonal coordinates for X in Angstroms") *:
      col[Real_8_3](31, 38, "x", "Orthogonal coordinates for X in Angstroms") *:
      col[Real_8_3](31, 38, "x", "Orthogonal coordinates for X in Angstroms") *:
      col[Real_6_2](55, 60, "occupancy", "Occupancy.") *:
      col[Real_6_2](61, 66, "tempFactor", "Temperature factor.") *:
      col[LString_2](77, 78, "element", "Element symbol, right-justified.") *:
      col[LString_2](79, 80, "charge", "Charge on the atom.") *:
      ()
    )
  }


  def parser = apply[LineParser, ParseCS[String], ParseCS]

}
*/
