package pdb

trait LinesParser[T] {
  def parse(lines: List[CharSequence]): (List[CharSequence], T)
}

// def lines[T](p: LineParser[T]) = {
//   def rec(lines: List[CharSequence], ts: Seq[T]): (List[CharSequence], Seq[T]) = lines match {
//     case Nil => (Nil, ts)
//     case l::ls => p.parse(l) match {
//       case LineResult.Reject => (lines, ts)
//       case LineResult.Accept(t) => rec(ls, ts ++ t)
//     }
//   }

//   rec(_, Seq.empty)
// }

/** The result of attempting to parse a line with a line parser.
 *
 * Parsers may accept a line as being theirs, or reject it and leave it to another handler to process.
 */
/*
enum LineResult[+T] {
  case Accept(value: Option[T])
  case Reject extends LineResult[Nothing]

  def map[U](f: T => U): LineResult[U] = this match {
    case Accept(v) => Accept(v map f)
    case Reject => Reject
  }
  def mapOption[U](f: T => Option[U]): LineResult[U] = this match {
    case Accept(v)  => Accept(v flatMap f)
    case Reject     => Reject
  }
}




@FunctionalInterface
trait LineParser[T] {
  def parse(cs: CharSequence): LineResult[T]
  def map[U](f: T => U): LineParser[U] = parse(_).map(f)
}


@FunctionalInterface
trait ParseCS[T] {
  def parse(cs: CharSequence): Option[T]
  def map[U](f: T => U): ParseCS[U] = parse(_).map(f)
  def mapOption[U](f: T => Option[U]): ParseCS[U] = parse(_).flatMap(f)
  def mapTry[U](f: T => U): ParseCS[U] = cs => try {
    parse(cs).map(f)
  } catch {
    case e : Exception => None
  }
  def filter(p: T => Boolean): ParseCS[T] = parse(_).filter(p)
  def withDefault(d: T): ParseCS[T] = parse(_).orElse(Some(d))
}

@FunctionalInterface
trait ParseSS[T] {
  def parse(charStart: Int, charEnd: Int, cs: CharSequence): Option[T]
  def map[U](f: T => U): ParseSS[U] = (s, e, c) => parse(s, e, c).map(f)
}

implied PdbRecordParser for PdbRecord[LineParser, ParseCS[String], ParseCS] {
  def fromName(n: ParseCS[String]): LineParser[String] = n.parse(_).fold(LineResult.Reject)(n => LineResult.Accept(Some(n)))

  def (lhs: LineParser[A]) :+ [A, B] (rhs: ParseCS[B]): LineParser[(A, B)] =
    chars => lhs.parse(chars) mapOption (a => rhs.parse(chars) map(b => (a, b)))
}



implied PdbRecordNameParser for PdbRecordName[ParseCS[String]] =
 (charStart, charEnd, name) => chars =>
   Some(chars.subSequence(charStart - 1, charEnd).toString).filter(_ == name)

implied PdbColumnParser[T] given ParseSS[T] for PdbColumn[ParseCS, T] =
  (charStart, charEnd, field, definition) =>
    the[ParseSS[T]].parse(charStart, charEnd, _)


implied for ParseCS[CharSequence] = Some(_)

implied for ParseCS[String] =
  the[ParseCS[CharSequence]].map(_.toString)

implied for ParseCS[Char] =
  the[ParseCS[CharSequence]].filter(_.length == 1).map(_.charAt(0))

implied for ParseCS[Int] = 
  the[ParseCS[String]].mapTry(_.toInt)

implied for ParseCS[Double] = 
  the[ParseCS[String]].mapTry(_.toDouble)

implied SSfromSC[T] given ParseCS[T] for ParseSS[T] = (charStart, charEnd, chars) => 
  the[ParseCS[T]].parse(chars.subSequence(charStart - 1, charEnd))
*/
