package pdb


type ResidueName = String
type AChar = Char         // alphabetic char?
type LString_2 = String   // String length 2
type LString_3 = String   // String length 3
type Atom = String        // atom code e.g. O=Oxygen
type Real_8_3 = Double    // number with 8.3 digits
type Real_6_2 = Double    // number with 6.2 digits